import getAllCards from "../API/getAllCards.js";
import Card from "../classes/Card.js";
import { CARDS_CONTAINER } from "../constants/constants.js";

const filterCard = async (
  searchTerm,
  flag,
  inputSearchValue,
  statusValue,
  priorityValue
) => {

  const cards = await getAllCards();

  const searchValue = searchTerm.toLowerCase();

  const filterData = cards.filter(
    ({ diseases, doctor, fullName, purpose, descriptionVisit, priority, status }) => {

      if (flag === "search") {
        return (
          diseases?.toLowerCase().includes(searchValue) ||
          doctor?.toLowerCase().includes(searchValue) ||
          fullName?.toLowerCase().includes(searchValue) ||
          purpose?.toLowerCase().includes(searchValue) ||
          descriptionVisit?.toLowerCase().includes(searchValue)
        );
      } 
      if (flag === "priority") {
        return (priority.toLowerCase() === priorityValue.toLowerCase() ||
                priorityValue === "Виберіть терміновість візиту");
      }
      if (flag === "status") {
          return (status.toLowerCase() === statusValue.toLowerCase() ||
                  statusValue === "Виберіть статуc візиту");
      }
    }
  );

  document.querySelector(CARDS_CONTAINER).innerHTML = "";

  filterData.forEach((element) => {

    if ((priorityValue === "Виберіть терміновість візиту" || element.priority === priorityValue) && 
        (statusValue === "Виберіть статуc візиту" || element.status === statusValue) && 
        (inputSearchValue === "" || element.doctor?.toLowerCase().includes(inputSearchValue) || 
                                    element.diseases?.toLowerCase().includes(inputSearchValue) ||
                                    element.fullName?.toLowerCase().includes(inputSearchValue) ||
                                    element.purpose?.toLowerCase().includes(inputSearchValue) ||
                                    element.descriptionVisit?.toLowerCase().includes(inputSearchValue))) {

      new Card(element).render(CARDS_CONTAINER);

    } else {return false};
  });
};

export default filterCard;


