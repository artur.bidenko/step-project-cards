import { NO_ITEMS_WRAPPER } from "../constants/constants.js";
import getAllCards from "../API/getAllCards.js";

const checkUserCards = async () => {
	const noItemsMessage = document.querySelector(NO_ITEMS_WRAPPER);
	const cards = await getAllCards();

	noItemsMessage.style.display = cards.length === 0 ? 'block' : '';
}

export default checkUserCards;