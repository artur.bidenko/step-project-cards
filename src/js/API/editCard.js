import instance from "./instance.js";

const editCard = async (cardId, body) => {
	try {
		return await instance.put(`/${cardId}`, body);
	} catch(error) {
		console.log(error);
	}
}

export default editCard;