import Form from "./Form.js";

export default class SelectDoctor extends Form {
    constructor () {
        super();
        this.selectDoctor = document.createElement('select');
    }

    createElements() {
        super.createElements();
		this.form.className = 'mb-3';
        this.selectDoctor.className = 'select__doctor form-select';

        this.selectDoctor.innerHTML = `
            <option selected>Вибрати лікаря:</option>
            <option value="cardiologist">Кардіолог</option>
            <option value="dentist">Стоматолог</option>
            <option value="therapist">Терапевт</option>
        `;

        this.form.append(this.selectDoctor);
    }
}